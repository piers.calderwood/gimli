FROM node:12.2.0

#COPY lib /code/lib
COPY . /code

WORKDIR /code
RUN yarn install

# ENV SELENIUM_BROWSER chrome:74:LINUX
ENV SELENIUM_REMOTE_URL http://localhost:4444/wd/hub
CMD [ "npm", "test" ]