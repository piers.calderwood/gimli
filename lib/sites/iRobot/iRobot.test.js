"use strict"
const {describe, it, after, before} = require('mocha');
const Page = require('./iRobotBasePage');
const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;
const chaiAsPromised = require('chai-as-promised');
chai.should();
chai.use(chaiAsPromised);

const utils = require('../../utils');

const config = require('../../config');
const productPage = config.iRobot.NA.productPage || "https://store.irobot.com/default/roomba-vacuuming-robot-vacuum-irobot-roomba-690/R690020.html?_ga=2.157257275.1841746714.1560268774-1132645849.1560268774";
const cartPage = config.iRobot.NA.cartPage;
const isActive = config.iRobot.NA.isActive || false;



(async function tests() {
    try{
        (isActive ? describe : describe.skip)("iRobot NA - Onsite present, cookie set", async function(){
            this.slow(60*1000); // yellow scares people. Slow website is fine.
            this.timeout(60 * 1000);
            let driver, page;

            beforeEach(async () =>{
                page = new Page();
                driver = page.driver;
                driver.manage().window().setSize(1200, 1300)
                await page.visit(productPage);
                await page.driver.sleep(1000); // force wait after render for any js to finish running.
            });

            afterEach(async() =>{
                await page.quit();
            });

            it('Find a product, add to cart, move to cart, then attempt to get the popup', async () =>{
                await page.tryAddToCart()
                await page.visit(cartPage);
                await page.driver.sleep(1000);
                await page.saveScreenshot("iRobotNA-01.png");

           
                await driver.sleep(2500);
                
                await page.triggerOnsite();

                await driver.sleep(3500);
                await page.saveScreenshot("iRobotNA-02.png");

                const onsiteSelector = 'div.addshop-active.fullHeight';
                let onsitePromise = page.findByCss(onsiteSelector);
                let expectedCookie = page.driver.manage().getCookie('cybSessionID');

                return Promise.all([
                    onsitePromise.should.eventually.be.ok,
                    expectedCookie.should.eventually.be.ok,
                ]);
            });
        });
    } catch(ex) {
        console.log(new Error(ex.message));
    }
})()