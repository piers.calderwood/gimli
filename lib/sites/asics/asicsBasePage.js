"use strict"

let Page = require('../../chromePage');

// Expand with necessary helper methods for this website.

class AsicsPage extends Page {
    constructor(){
        super()
    }
    
    async closeCountryModal() {
        try {
            let countryModalCloseSelector = 'div[onclick][id="_tealiumModalClose"]';
            let countryModal = await this.findByCss(countryModalCloseSelector, 1000);
            await countryModal.click();
        }
        catch(ex){
            // Eat the exception since it was probably a not found or a not interactable.
            return;
        }
    }

    async closeMembershipPromoModal() {
        const selector = '.ui-dialog [type=button].ui-button';
        let closeButton = await this.findByCss(selector);
        await closeButton.click();
    }

    async visitAProductPage() {
        const mensMenuListSelector = '.pt_storefront .top-banner .menu-category.level-1>li.status-0';
        const featuredMenuItemSelector = mensMenuListSelector + ' ' +'ul:nth-child(1) li:nth-child(1) a[data-level="2"]';
    
        let menutItem = this.findByCss(mensMenuListSelector);
        var action = this.driver.actions();
        await action.mouseMove(await menutItem).perform();
        
        // Can't locate an item until it is visible.
        let featuredItem = await this.findByCss(featuredMenuItemSelector);
        action = this.driver.actions();
        await action.mouseMove(featuredItem).click(featuredItem).perform();
    
        await this.driver.sleep(5 * 1000); // Ideally you would visit the href instead.
        // We could just get the href value and then driver.visit(), however this will demo the mouse movement api in case it's not possible via SPA
    
        const firstProductSelector = '.pt_product-search-result [id="main"] .search-container [id="primary"].primary-content .search-result-content>ul>li:nth-child(1)';
        const firstProductATagSelector = firstProductSelector + ' ' + 'a:nth-child(1)[data-productid]';
    
        let productATag = await this.findByCss(firstProductATagSelector);
        let productUrl = productATag.getAttribute('href');
    
        return this.visit(await productUrl);
    }

    async selectValidVariation(){
        const variantsContainerSelector = '.product-variation-container';
        const variantsListSelector = variantsContainerSelector + '>div>ul';
    
        const colorVariantSelector  = variantsListSelector + '>li:nth-child(1) li[data-instock="true"] a';
        const widthVariantSelector  = variantsListSelector + '>li:nth-child(2) li[data-instock="true"] a';
        const sizeVariantSelector   = variantsListSelector + '>li:nth-child(3) li[data-instock="true"] a';
    
        // These need to run synchronously since in-stock depends on the previous selection
        await this.findByCss(colorVariantSelector, 2000).then(el => el.click());
        await this.findByCss(widthVariantSelector, 5000).then(el => el.click());
        await this.findByCss(sizeVariantSelector,  5000).then(el => el.click());
    }

    async addToCart() {
        const addToCartButtonSelector = '.product-detail button[title*="Cart"]';
        let addToCartButton = await this.findByCss(addToCartButtonSelector);
        await addToCartButton.click();
    }
}

module.exports = AsicsPage;