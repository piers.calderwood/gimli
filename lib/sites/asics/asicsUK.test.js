"use strict"

const {describe, it, after, before} = require('mocha');

const Page = require('./asicsBasePage');
const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
const utils = require('../../utils');

const config = require('../../config');
const homePageUrl = config.asics.UK.homePageUrl || 'https://www.asics.com/gb/en-gb/';
const cartUrl = config.asics.UK.cartUrl || 'https://www.asics.com/gb/en-gb/cart/';
const isActive = config.asics.UK.isActive || false;

(async function tests() {
    try{
        (isActive ? describe : describe.skip)("Asics UK - Onsite present", async function(){
            this.timeout(60 * 1000);
            let driver, page;

            beforeEach(async () =>{
                page = new Page();
                driver = page.driver;
                await page.visit(homePageUrl);
                await page.driver.sleep(2 * 1000); // force 5s wait after render for any js to finish running.
            });

            afterEach(async() =>{
                await page.quit();
            });

            // Skipping this test until asic enabled
            it('Find a product, add to cart, move to cart, then attempt to get the popup', async () =>{
                await page.saveScreenshot("asicsUK-01.png");

                await page.closeMembershipPromoModal();
                await page.visitAProductPage();

                await page.closeCountryModal();

                await page.selectValidVariation();
                await page.addToCart();

                await page.visit(cartUrl);
                await page.driver.sleep(2 * 1000); //remove me and replace with some asserts
                await page.saveScreenshot("asicsUK-02.png");
            });
        });
    } catch(ex) {
        console.log(new Error(ex.message));
    }
})()