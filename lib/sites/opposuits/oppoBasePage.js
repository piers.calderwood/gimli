"use strict"

let Page = require('../../chromePage');

const variantFormSelector = '.product-view .panel-body>form';

class OppoPage extends Page {
    constructor(){
        super()
    }

    async selectVariant() {
        const sizeVariantSelector = variantFormSelector + ' ' + 'select option:nth-child(2)';
    
        let sizeSelection = await this.findByCss(sizeVariantSelector);
    
        await sizeSelection.click();
    }

    async addToCart() {
        const addToBasketButtonSelector = variantFormSelector +  ' ' + 'button[title*="asket"]';
    
        let basketButton = await this.findByCss(addToBasketButtonSelector);
    
        await basketButton.click();
    }

    async triggerOnsite() {
        let size = await this.driver.manage().window().getSize();
        var x = size.width - 2;
        var y = 20;
    
        var action = this.driver.actions();
        await action.mouseMove({x, y}).click().perform();
        this.driver.sleep(50);       
    
        //return this.driver.sleep(3000);
        let accountDropdown = await this.findByCss('.dropdown-account');
        await accountDropdown.click();
    
        action = this.driver.actions();
        x = 0;
        y = -500;
        await action.mouseMove({x, y}).perform();
    
    
        const onsiteSelector = 'div.addshop-active.fullHeight';
    }
}

module.exports = OppoPage;