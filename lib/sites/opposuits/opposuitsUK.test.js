"use strict"
const {describe, it, after, before} = require('mocha');
const Page = require('./oppoBasePage');
const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
chai.should();
chai.use(chaiAsPromised);

const utils = require('../../utils');

const config = require('../../config');
const productPage = config.oppo.UK.productPage || "https://www.opposuits.co.uk/suits/tuxedos/jet-set-black.html";
const isActive = config.oppo.UK.isActive || false;
if(!isActive) {
    console.log("Not running Oppo tests");
}

(async function tests() {
    try{
        (isActive ? describe : describe.skip)("OppoSuits UK - Onsite present, cookie set", async function(){
            this.slow(60*1000); // yellow scares people. Slow website is fine.
            this.timeout(60 * 1000);
            let driver, page;

            beforeEach(async () =>{
                page = new Page();
                driver = page.driver;
                driver.manage().window().setSize(1200, 1300)
                await page.visit(productPage);
                await page.driver.sleep(5 * 1000); // force 5s wait after render for any js to finish running.
            });

            afterEach(async() =>{
                await page.quit();
            });

            it('Find a product, add to cart, move to cart, then attempt to get the popup', async () =>{
                await page.saveScreenshot("OppoUK-01.png");

                await page.selectVariant();
                await page.addToCart();
                await page.triggerOnsite();

                await page.driver.sleep(2500);
                await page.saveScreenshot("OppoUK-02.png");

                const onsiteSelector = 'div.addshop-active.fullHeight';
                let onsitePromise = page.findByCss(onsiteSelector);
                let expectedCookie = page.driver.manage().getCookie('cybSessionID');

                return Promise.all([
                    onsitePromise.should.eventually.be.ok,
                    expectedCookie.should.eventually.be.ok,
                ]);
            });
        });
    } catch(ex) {
        console.log(new Error(ex.message));
    }
})()