"use strict"
require.cache[require.resolve('../../chromePage')];
let Page = require('../../chromePage');

const productOptionsSelector = '[id="product-options"]'

class KoioPage extends Page{
    constructor(){
        super()
    }

    async selectVariant() {
        if(await this.tryAddToCart()){
            return true;
        }
        //This is buggy and error prone. Koio  should defuault to a valid variant if one isn't specified in the URL
        var sizes = await this.allSizes();
        for(var i=0; i < sizes.length; i++) {
            sizes = await this.allSizes()
            let size = sizes[i];
            await size.click();
            // await this.tryCloseModalKoio(4000);
            if(await this.tryAddToCart()) {
                return true;
            }
        }
        return false;
    }

    async allColors() {
        const colorListSelector = productOptionsSelector + " .product-color-option__list";
        let colorOptionsSelector = colorListSelector + " > a";
    
        return await this.findManyByCss(colorOptionsSelector);
    }
    
    async allSize() {
        const sizeListSelector = productOptionsSelector + " .product-option-row--size";
        let sizesSelector = sizeListSelector + " .swatch-element";
    
        return await this.findManyByCss(sizesSelector);
    }
    
    async tryAddToCart() {
        let addToBagButton = await this.findById("add-to-cart");
        let disabledValue = await addToBagButton.getAttribute("disabled");
        // console.log(disabledValue);
        // console.log(typeof(disabledValue));
        if(disabledValue != null && (disabledValue == "disabled" || disabledValue == "true" || disabledValue)) {
            return false;
        }
        await addToBagButton.click();
        return true;
    }
    
    async triggerOnsite() {
        let cartCloseSelector = "#minicart-container .minicart__header span";
        let size = await this.driver.manage().window().getSize();
        var x = size.width - 2;
        var y = 20;
    
        var action = this.driver.actions();
        await action.mouseMove({x, y}).click().perform();
        this.driver.sleep(50);

        let cartClose = await this.findByCss(cartCloseSelector);
        await cartClose.click();
    
        action = this.driver.actions();
        x = 0;
        y = -500;
        await action.mouseMove({x, y}).perform();
    
    
        const onsiteSelector = 'div.addshop-active.fullHeight';
    }
}

module.exports = KoioPage;