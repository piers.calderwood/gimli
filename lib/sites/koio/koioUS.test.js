"use strict"
const {describe, it, after, before} = require('mocha');
const Page = require('./koioBasePage');
const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;
const chaiAsPromised = require('chai-as-promised');
chai.should();
chai.use(chaiAsPromised);

const utils = require('../../utils');

const config = require('../../config');
const productPage = config.koio.US.productPage || "https://www.koio.co/collections/collection-women/products/capri-triple-white-women?variant=5382335823908";
const isActive = config.koio.US.isActive || false;



(async function tests() {
    try{
        (isActive ? describe : describe.skip)("Koio US - Onsite present, cookie set", async function(){
            this.slow(60*1000); // yellow scares people. Slow website is fine.
            this.timeout(60 * 1000);
            let driver, page;

            beforeEach(async () =>{
                page = new Page();
                driver = page.driver;
                driver.manage().window().setSize(1200, 1300)
                await page.visit(productPage);
                await page.driver.sleep(1000); // force wait after render for any js to finish running.
            });

            afterEach(async() =>{
                await page.quit();
            });

            it('Find a product, add to cart, move to cart, then attempt to get the popup', async () =>{
                await page.saveScreenshot("koioUS-01.png");

                if(! await page.selectVariant() ){
                    assert.fail("Could not locate valid variant.");
                }
                await driver.sleep(2500);
                await page.triggerOnsite();

                await driver.sleep(3500);
                await page.saveScreenshot("koioUS-02.png");

                const onsiteSelector = 'div.addshop-active.fullHeight';
                let onsitePromise = page.findByCss(onsiteSelector);
                let expectedCookie = page.driver.manage().getCookie('cybSessionID');

                return Promise.all([
                    onsitePromise.should.eventually.be.ok,
                    expectedCookie.should.eventually.be.ok,
                ]);
            });
        });
    } catch(ex) {
        console.log(new Error(ex.message));
    }
})()