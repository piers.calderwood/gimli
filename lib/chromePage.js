const {Builder, By, until} = require('selenium-webdriver');

const chrome = require('selenium-webdriver/chrome');
const config = require('../lib/config');

var chromePage = function() {
    this.driver = buildChromeDriver();
    
    this.visit = async function(url) {
        return await this.driver.get(url);
    }

    this.quit = async function() {
        return await this.driver.quit();
    }

    this.findById = async function(id, timeout=15000) {
        await this.driver.wait(until.elementLocated(By.id(id)), timeout, 'Looking for element');
        return await this.driver.findElement(By.id(id));
    };

    this.findByName = async function(name, timeout=15000) {
        await this.driver.wait(until.elementLocated(By.name(name)), timeout, 'Looking for element');
        return await this.driver.findElement(By.name(name));
    };

    this.findByClass = async function(className, timeout=15000) {
        await this.driver.wait(until.elementLocated(By.className(className)), timeout, 'Looking for element');
        return await this.driver.findElement(By.className(className));
    };

    this.findByCss = async function(cssSelector, timeout=15000) {
        await this.driver.wait(until.elementLocated(By.css(cssSelector)), timeout, 'Looking for element');
        return await this.driver.findElement(By.css(cssSelector));
    };

    this.findManyById = async function(id, timeout=15000) {
        await this.driver.wait(until.elementLocated(By.id(id)), timeout, 'Looking for element');
        return await this.driver.findElements(By.id(id));
    };

    this.findManyByName = async function(name, timeout=15000) {
        await this.driver.wait(until.elementLocated(By.name(name)), timeout, 'Looking for element');
        return await this.driver.findElements(By.name(name));
    };

    this.findManyByClass = async function(className, timeout=15000) {
        await this.driver.wait(until.elementLocated(By.className(className)), timeout, 'Looking for element');
        return await this.driver.findElements(By.className(className));
    };

    this.findManyByCss = async function(cssSelector, timeout=15000) {
        await this.driver.wait(until.elementLocated(By.css(cssSelector)), timeout, 'Looking for element');
        return await this.driver.findElements(By.css(cssSelector));
    };

    this.write = async function (el, txt) {
        return await el.sendKeys(txt);
    };

    this.saveScreenshot = async function(name){
        b64Image = await this.driver.takeScreenshot();
        if(name.length < 4 || name.substring(name.length - 4) !== '.png'){
            name = name + '.png';
        }
        name = './report-images/'+name;
        console.log("Saved image to " + name);
        require('fs').writeFile(name, b64Image, 'base64', function(err) {
            if(err) {
                console.log('Issue saving image '+ err);
            }
        });
    }
};

function buildChromeDriver(){
    let options = new chrome.Options();
    let chromeConfig = config.browserConfigs.chrome;
    let browserName = chromeConfig.browserName
    let browserVersion = chromeConfig.browserVersion
    let browserOS = chromeConfig.browserOS
    chromeConfig.browserArguments.forEach(function(arg){
        options.addArguments(arg);
    });
    options.setUserPreferences({ credential_enable_service: false });


    this.driver = new Builder()
        .forBrowser(browserName, browserVersion, browserOS)
        .setChromeOptions(options);
    if(chromeConfig.useRemoteWebdriver){
        driver.usingServer(chromeConfig.remoteHubEndpoint);
    }        
    return driver.build();
}

module.exports = chromePage;