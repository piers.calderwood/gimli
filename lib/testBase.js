"use strict"
// I'd like this capture a lot of common test function, like beforeEach and afterEach.

class TestBase {
    constructor(Page){
        super()
    }

    async defaultSetup() {
        page = new Page();
        driver = page.driver;
        driver.manage().window().setSize(1200, 1300)
        await page.visit(productPage);
        await page.driver.sleep(1000); // force wait after render for any js to finish running.
    }
}

