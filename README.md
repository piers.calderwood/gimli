## Installation:
### Requirements:
1. node v12.2.0
    * `yarn` is used for package management

2. `docker` and `docker-compose`

2. Or `ChromeDriver=74.0.3729.6` and `chrome` [](https://www.google.com/chrome/static/images/favicons/favicon.ico) on the same versions

#### 1. Docker
Docker is the preferred method of running the tests as it provides environment consistenty.

If you cannot use docker or are on a Mac, which doesn't support `network=host`, then you can use the alternative

If docker & docker-compose are installed it should be as easy as running 
> `$> cd gimli`

> `$> docker-compose build`

> `$> docker-compose up`

By default, reports are output into `~/gimli-out/`


#### 2. Local runtime
http://chromedriver.chromium.org/
Download the compatible driver `74.0.3729.6` and add it to your `PATH`

Adjust the configuration file `/lib/config.json` to work with the  driver.
    * Change `browserOS` to the correct system
    * `useRemoteWebdriver` to `false`
    * Optionally, disable `headless` while developing.
    
    
### Running
> `$> npm test`